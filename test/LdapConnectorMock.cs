﻿using System.DirectoryServices.Protocols;
using System.Linq;
using LdapInjectionStory.LdapConnection;
using LinqToLdap;
using Moq;

namespace LdapInjectionStoryTests
{
    public class LdapConnectorMock
    {
        public static string DummyLogin = "test";
        public static string DummyPassword = "test";

        public static ILdapConnector GetLdapConnector(
            Mock<IDirectoryContext> directoryContextMock)
        {
            var mock = new Mock<ILdapConnector>();
            mock
                .Setup(c => c.GetConnection())
                .Returns(directoryContextMock.Object);
            return mock.Object;
        }

        public static IQueryable<User> GetEmptyUsers() => (new User[0]).AsQueryable();
        public static IQueryable<User> GetOnlyLoginUser() => (new []
        {
            new User { Login = DummyLogin }
        }).AsQueryable();
        public static IQueryable<User> GetValidUser() => (new []
        {
            new User { Login = DummyLogin, Password = DummyPassword }
        }).AsQueryable();
        public static IQueryable<User> GetRepeatingUsers() => (new []
        {
            new User { Login = DummyLogin, Password = DummyPassword },
            new User { Login = DummyLogin, Password = DummyPassword }
        }).AsQueryable();

        public static Mock<IDirectoryContext> GetDirectoryContextMock(
            IQueryable<User> resultQueryable)
        {
            var directoryContextMock = new Mock<IDirectoryContext>();
            directoryContextMock
                .Setup(d => d.Query<User>(It.IsAny<SearchScope>(), It.IsAny<string>()))
                .Returns(resultQueryable);
            return directoryContextMock;
        }
    }
}