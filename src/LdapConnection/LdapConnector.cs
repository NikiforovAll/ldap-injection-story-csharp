﻿using System;
using System.DirectoryServices.Protocols;
using System.Net;
using LinqToLdap;

namespace LdapInjectionStory.LdapConnection
{
    public interface ILdapConnector
    {
        IDirectoryContext GetConnection();
    }

    public class LdapConnector : ILdapConnector
    {
        private static string  AdServer = "127.0.0.1";
        private static int  AdPort = 1389;
        private static string  AdUser = @"cn=admin,dc=example,dc=org";
        private static string  AdPassword = "adminpassword";
        private static Lazy<LdapConfiguration> LdapConfiguration
            = new Lazy<LdapConfiguration>(() => GetLdapConfiguration());
        private static NetworkCredential NetworkCredential = new NetworkCredential(AdUser, AdPassword);

        public IDirectoryContext GetConnection() => new DirectoryContext(LdapConfiguration.Value);

        private static LdapConfiguration GetLdapConfiguration()
        {
            var config = new LdapConfiguration()
                .MaxPageSizeIs(5)
                .AddMapping(new UserMapping());
            
            config
                .ConfigureFactory(AdServer)
                .UsePort(AdPort)
                .AuthenticateAs(NetworkCredential)
                .AuthenticateBy(AuthType.Basic)
                .ProtocolVersion(3);
                
            return config.UseStaticStorage();
        }
    }
}