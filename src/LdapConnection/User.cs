﻿namespace LdapInjectionStory.LdapConnection
{
    public class User
    {
        public const string NamingContext = "ou=users,dc=example,dc=org";
        public string Login { get; set; }
        public string Password { get; set; }
    }
}