﻿using System.Collections.Generic;
using LinqToLdap.Mapping;

namespace LdapInjectionStory.LdapConnection
{
    public class UserMapping : ClassMap<User>
    {
        public override IClassMap PerformMapping(
            string namingContext = null, 
            string objectCategory = null,
            bool includeObjectCategory = true,
            IEnumerable<string> objectClasses = null,
            bool includeObjectClasses = true)
        {
            NamingContext(User.NamingContext);
            Map(x => x.Login).Named("cn").ReadOnly();
            Map(x => x.Password).Named("userPassword").ReadOnly();
            return this;
        }
    }
}