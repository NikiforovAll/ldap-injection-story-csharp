﻿using System.Linq;
using LdapInjectionStory.LdapConnection;

namespace LdapInjectionStory
{
    public class PasswordChecker
    {
        public const string UserPasswordAttribute = "userPassword";
        private readonly ILdapConnector _ldapConnector;
        public PasswordChecker(ILdapConnector ldapConnector) => _ldapConnector = ldapConnector;
        
        public bool Check(string login, string password)
        {
            using var connection = _ldapConnector.GetConnection();
            var user = connection.Query<User>().Single(u => u.Login == login);
            return user.Password == password;
        }
    }
}