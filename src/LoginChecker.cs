﻿using System.Linq;
using LdapInjectionStory.LdapConnection;

namespace LdapInjectionStory
{
    public class LoginChecker
    {
        private readonly ILdapConnector _ldapConnector;
        public LoginChecker(ILdapConnector ldapConnector) => _ldapConnector = ldapConnector;

        public bool Check(string login)
        {
            using var connection = _ldapConnector.GetConnection();
            return connection.Query<User>().Any(u => u.Login == login);
        }
    }
}